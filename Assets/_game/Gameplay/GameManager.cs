﻿using System;
using System.Collections.Generic;
using _Game.Card;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace _Game.Gameplay
{
    public class GameManager : MonoBehaviour
    {
        public int minCardsCount = 4;
        public int maxCardsCount = 6;

        [Space]
        public GameZone hand;
        public GameZone battlefield;

        [Space]
        public GameObject cardPrefab;
        public ProgressBar progressBar;
        public Transform fillAnimationStartPosition;

        private Dictionary<CardObject, GameZone> _cardToZone = new Dictionary<CardObject, GameZone>();

        private int _nextCardIndex = 0;
        private bool _drawComplete = false;

        private const int MinStatValue = -2;
        private const int MaxStatValue = 9;

        private const float HandDrawAnimationDelay = 0.2f;

        private async void Start()
        {
            var cardsCount = Random.Range(minCardsCount, maxCardsCount + 1);

            await ArtCache.LoadTexturesToCache(1, new Progress<float>(v => progressBar.Report(v)));
            progressBar.gameObject.SetActive(false);

            await DrawHand(cardsCount);
            _drawComplete = true;
        }

        private async UniTask DrawHand(int cardsCount)
        {
            for (var i = 0; i < cardsCount; i++)
            {
                var cardData = CardRandomized.GetRandom();
                var card = Instantiate(
                        cardPrefab,
                        fillAnimationStartPosition.position,
                        Quaternion.identity,
                        hand.transform
                    )
                    .GetComponent<CardObject>();
                card.Init(cardData);
                card.DragEnd += OnCardDragEnd;
                hand.AddCard(card);
                _cardToZone[card] = hand;

                await UniTask.Delay(TimeSpan.FromSeconds(HandDrawAnimationDelay));
            }
            await hand.RepositionAnimation.ToUniTask();
        }

        public void RandomizeNextCard()
        {
            if (!_drawComplete || hand.Size == 0)
            {
                return;
            }

            var statIndex = Random.Range(1, 4);
            var cardIndex = _nextCardIndex % hand.Size;
            var card = hand.GetCardByIndex(cardIndex);
            var cardData = card.Data;

            int oldStatValue;
            switch (statIndex)
            {
                case 1:
                    oldStatValue = cardData.Attack;
                    break;
                case 2:
                    oldStatValue = cardData.Health;
                    break;
                case 3:
                    oldStatValue = cardData.Manacost;
                    break;
                default:
                    throw new ArgumentException();
            }

            int statValue;
            do
            {
                statValue = Random.Range(MinStatValue, MaxStatValue);
            } while (statValue == oldStatValue);

            switch (statIndex)
            {
                case 1:
                    cardData.Attack = statValue;
                    break;
                case 2:
                    cardData.Health = statValue;
                    break;
                case 3:
                    cardData.Manacost = statValue;
                    break;
                default:
                    throw new ArgumentException();
            }
            _nextCardIndex++;

            if (cardData.Health <= 0)
            {
                _cardToZone.Remove(card);
                card.DragEnd -= OnCardDragEnd;
                card.CurrentAnimation.ToUniTask()
                    .ContinueWith(() =>
                    {
                        hand.RemoveCard(card);
                        Destroy(card.gameObject);
                    })
                    .Forget();
            }
        }

        private void OnCardDragEnd(CardObject card)
        {
            var cardRect = GetWorldRect( card.GetComponent<RectTransform>());
            var handRect = GetWorldRect(hand.GetComponent<RectTransform>());
            var battlefieldRect = GetWorldRect(battlefield.GetComponent<RectTransform>());
            var prevZone = _cardToZone[card];
            var nextZone = prevZone;
            if (handRect.Overlaps(cardRect))
            {
                nextZone = hand;
            }
            else if (battlefieldRect.Overlaps(cardRect))
            {
                nextZone = battlefield;
            }

            if (prevZone == nextZone)
            {
                nextZone.UpdatePositions();
            }
            else
            {
                _cardToZone[card] = nextZone;
                prevZone.RemoveCard(card);
                nextZone.AddCard(card);
            }
        }

        private Rect GetWorldRect(RectTransform t)
        {
            Vector3[] c = new Vector3[4];
            t.GetWorldCorners(c);
            var width = c[2].x - c[0].x;
            var height = c[1].y - c[0].y;
            return new Rect(c[0].x, c[0].y, width, height);
        }

        public void Restart()
        {
            if (!_drawComplete)
            {
                return;
            }
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
