﻿using System.Collections.Generic;
using _Game.Card;
using DG.Tweening;
using UnityEngine;

namespace _Game.Gameplay
{
    public class GameZone : MonoBehaviour
    {
        public float distanceBetweenCards = 200f;
        public float arcHeight = 100f;
        public float maxCardAngle = 15f;
        public float repositionAnimationTime = 0.3f;

        private readonly List<CardObject> _cards = new List<CardObject>();
        public int Size => _cards.Count;

        public Sequence RepositionAnimation { get; private set; }


        public void AddCard(CardObject card)
        {
            card.transform.SetParent(transform);
            _cards.Add(card);
            UpdatePositions();
        }

        public void RemoveCard(CardObject card)
        {
            _cards.Remove(card);
            UpdatePositions();
        }

        public CardObject GetCardByIndex(int index)
        {
            return _cards[index];
        }

        public void UpdatePositions()
        {
            RepositionAnimation?.Kill();
            RepositionAnimation = DOTween.Sequence();

            for (int i = 0; i < _cards.Count; i++)
            {
                var card = _cards[i];
                var (pos, rotation) = GetTargetCardPosition(i);
                var tween = MoveToPosition(card.transform, pos, rotation, repositionAnimationTime);
                tween.OnStart(() => card.Interactable = false)
                    .OnComplete(() => card.Interactable = true);
                RepositionAnimation.Join(tween);
            }
        }

        private Sequence MoveToPosition(Transform card, Vector2 position, Quaternion rotation, float time)
        {
            var positionAnimation = card.DOLocalMove(position, time);
            var rotationAnimation = card.DOLocalRotate(rotation.eulerAngles, time);
            var animations = DOTween.Sequence();
            animations.Append(positionAnimation).Join(rotationAnimation);
            return animations;
        }

        private (Vector2, Quaternion) GetTargetCardPosition(int index)
        {
            int cardsCount = _cards.Count;
            if (cardsCount <= 1)
            {
                return (new Vector2(0, arcHeight), Quaternion.identity);
            }


            float t = (float) index / (cardsCount - 1);
            float zoneWidth = cardsCount * distanceBetweenCards;
            float x = Mathf.Lerp(-zoneWidth / 2, zoneWidth / 2, t);
            float y = Mathf.Sin(Mathf.PI * t) * arcHeight;
            var position = new Vector2(x, y);

            float angle = Mathf.Lerp(maxCardAngle, -maxCardAngle, t);
            var rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            return (position, rotation);
        }
    }
}
