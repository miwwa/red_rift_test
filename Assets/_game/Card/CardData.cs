﻿using System;
using UnityEngine;

namespace _Game.Card
{
    [Serializable]
    public class CardData
    {
        public readonly int Id;
        public readonly Texture2D Art;
        public readonly string Title;
        public readonly string Description;

        [SerializeField]
        private int manacost;
        public int Manacost
        {
            get => manacost;
            set
            {
                if (manacost != value)
                {
                    ManacostChanged?.Invoke(manacost, value);
                    manacost = value;
                }
            }
        }

        [SerializeField]
        private int attack;
        public int Attack
        {
            get => attack;
            set
            {
                if (attack != value)
                {
                    AttackChanged?.Invoke(attack, value);
                    attack = value;
                }
            }
        }

        [SerializeField]
        private int health;
        public int Health
        {
            get => health;
            set
            {
                if (health != value)
                {
                    HealthChanged?.Invoke(health, value);
                    health = value;
                }
            }
        }

        public event Action<int, int> ManacostChanged;
        public event Action<int, int> AttackChanged;
        public event Action<int, int> HealthChanged;

        public CardData(int id, Texture2D art, string title, string description, int manacost, int attack, int health)
        {
            Id = id;
            Art = art;
            Title = title;
            Description = description;
            this.manacost = manacost;
            this.attack = attack;
            this.health = health;
        }
    }
}
