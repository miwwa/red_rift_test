﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace _Game.Card
{
    public static class ArtCache
    {
        public const int ArtWidth = 175;
        public const int ArtHeight = 150;
        public static readonly string ArtUrl = $"https://picsum.photos/{ArtWidth}/{ArtHeight}";

        private static Texture2D[] _cache;
        public static int Size => _cache?.Length ?? 0;

        public static Texture2D GetTexture(int id)
        {
            return _cache[id];
        }

        public static async UniTask LoadTexturesToCache(int texturesCount, IProgress<float> progress = null)
        {
            _cache = new Texture2D[texturesCount];
            for (var i = 0; i < texturesCount; i++)
            {
                _cache[i] = await DownloadRandomTexture();
                progress?.Report((float) i / texturesCount);
            }
            progress?.Report(1);
        }

        private static async UniTask<Texture2D> DownloadRandomTexture()
        {
            var request = await UnityWebRequestTexture.GetTexture(ArtUrl).SendWebRequest();
            if (request.isHttpError || request.isNetworkError)
            {
                Debug.LogError(request.error);
                return null;
            }
            return ((DownloadHandlerTexture) request.downloadHandler).texture;
        }
    }
}
