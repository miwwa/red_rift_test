﻿using System;
using _Game.UI;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Card
{
    [RequireComponent(typeof(CardDragAndDrop))]
    public class CardObject : MonoBehaviour
    {
        public RawImage art;
        public TMP_Text titleText;
        public TMP_Text descriptionText;
        public TMP_Text healthText;
        public TMP_Text attackText;
        public TMP_Text manacostText;

        [Space]
        public float counterAnimationDuration = 0.5f;

        public CardData Data { get; private set; }
        public Sequence CurrentAnimation { get; private set; }

        public bool Interactable
        {
            get => _dragAndDrop.interactable;
            set => _dragAndDrop.interactable = value;
        }

        public event Action<CardObject> DragStart;
        public event Action<CardObject> DragEnd;

        private bool _inited;
        private CardDragAndDrop _dragAndDrop;
        private ParticleSystem _particles;

        public void Init(CardData data)
        {
            if (_inited)
            {
                throw new Exception("Init can be called one time");
            }

            _dragAndDrop = GetComponent<CardDragAndDrop>();
            _particles = GetComponentInChildren<ParticleSystem>();

            Data = data;
            art.texture = Data.Art;
            titleText.text = Data.Title;
            descriptionText.text = Data.Description;
            healthText.text = Data.Health.ToString();
            attackText.text = Data.Attack.ToString();
            manacostText.text = Data.Manacost.ToString();

            Data.HealthChanged += OnHealthChanged;
            Data.AttackChanged += OnAttackChanged;
            Data.ManacostChanged += OnManacostChanged;

            _dragAndDrop.DragStart += OnDragStart;
            _dragAndDrop.DragEnd += OnDragEnd;

            _inited = true;
        }

        private void OnDestroy()
        {
            CurrentAnimation?.Kill();
            if (Data != null)
            {
                Data.HealthChanged -= OnHealthChanged;
                Data.AttackChanged -= OnAttackChanged;
                Data.ManacostChanged -= OnManacostChanged;
            }
            if (_dragAndDrop != null)
            {
                _dragAndDrop.DragStart -= OnDragStart;
                _dragAndDrop.DragEnd -= OnDragEnd;
            }
        }

        private void OnHealthChanged(int old, int next)
        {
            AnimateCounter(healthText, next);
        }

        private void OnAttackChanged(int old, int next)
        {
            AnimateCounter(attackText, next);
        }

        private void OnManacostChanged(int old, int next)
        {
            AnimateCounter(manacostText, next);
        }

        private void AnimateCounter(TMP_Text text, int newValue)
        {
            CurrentAnimation?.Kill();

            var counterAnimation = text.DOCounter(int.Parse(text.text), newValue, counterAnimationDuration)
                .OnKill(() => text.text = newValue.ToString());

            var startScale = text.transform.localScale;
            var endScale = startScale * 1.5f;
            var scaleAnimation = text.transform.DOScale(endScale, counterAnimationDuration)
                .SetLoops(2, LoopType.Yoyo)
                .OnKill(() => text.transform.localScale = startScale);

            CurrentAnimation = DOTween.Sequence();
            CurrentAnimation.Append(counterAnimation).Join(scaleAnimation);
        }

        private void OnDragStart()
        {
            _particles.Play();
            DragStart?.Invoke(this);
        }

        private void OnDragEnd()
        {
            _particles.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            DragEnd?.Invoke(this);
        }
    }
}
