﻿using _Game.Utils;
using UnityEngine;

namespace _Game.Card
{
    public static class CardRandomized
    {
        private static int _nextId = 0;

        private static readonly string[] Titles = new[]
        {
            "title 1",
            "title 2",
            "title 3",
            "title 4",
            "title 5",
            "title 6",
            "title 7",
            "title 8",
            "title 9",
            "title 10",
        }.Shuffle();

        private static readonly string[] Descriptions = new[]
        {
            "description 1",
            "description 2",
            "description 3",
            "description 4",
            "description 5",
            "description 6",
            "description 7",
            "description 8",
            "description 9",
            "description 10",
        }.Shuffle();

        public static CardData GetRandom()
        {
            var id = _nextId;
            _nextId++;
            return new CardData(
                id,
                ArtCache.GetTexture(id % ArtCache.Size),
                Titles[id % Titles.Length],
                Descriptions[id % Descriptions.Length],
                Random.Range(1, 10),
                Random.Range(1, 10),
                Random.Range(1, 10)
            );
        }
    }
}
