﻿using System;
using _Game.Utils;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Game.UI
{
    public class CardDragAndDrop : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        public bool interactable;

        public event Action DragStart;
        public event Action DragEnd;

        private static Transform _dragLayer;

        private RectTransform _rectTransform;
        private RectTransform _canvasRect;
        private Camera _camera;

        private Transform _originalLayer;
        private Vector2 _prevDragPosition;

        private void Awake()
        {
            _camera = Camera.main;
            _rectTransform = GetComponent<RectTransform>();
            _canvasRect = transform.root.GetComponent<RectTransform>();

            if (_dragLayer == null)
            {
                _dragLayer = FindObjectOfType<DragLayer>().transform;
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (!interactable)
            {
                return;
            }

            _originalLayer = transform.parent;
            _rectTransform.SetParent(_dragLayer);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                _canvasRect,
                eventData.position,
                _camera,
                out _prevDragPosition
            );
            DragStart?.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!interactable)
            {
                return;
            }

            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                _canvasRect,
                eventData.position,
                _camera,
                out var newPosition
            );
            var delta = newPosition - _prevDragPosition;
            _prevDragPosition = newPosition;
            _rectTransform.anchoredPosition += delta;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (!interactable)
            {
                return;
            }

            _rectTransform.SetParent(_originalLayer);
            DragEnd?.Invoke();
        }
    }
}
