﻿using UnityEngine;

namespace _Game
{
    public class ProgressBar : MonoBehaviour
    {
        public Transform bar;

        public void Report(float progress)
        {
            var scale = bar.localScale;
            scale.x = progress;
            bar.localScale = scale;
        }
    }
}
